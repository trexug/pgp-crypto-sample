﻿using System.Text;

namespace CryptoSample;

public class Program
{
    private const string Workspace = @"C:\Files\Encryption-Playground";
    private const string PublicKeyName = "public.asc";
    private const string PrivateKeyName = "private.asc";

    private const string SecretTextName = "secret-text.asc";
    private const string SecretImageName = "secret-image.asc";

    private static CryptoManager _cryptoManager = new CryptoManager();
    
    public static async Task Main()
    {
        // Comment these in/out as needed
        
        //await Setup();
        //await Encrypt();
        await Decrypt();
    }

    private static async Task Encrypt()
    {
        var publicKeyBytes = await File.ReadAllBytesAsync(Path.Combine(Workspace, PublicKeyName));
        var publicKey = new RsaKey(publicKeyBytes);
        
        // **** Encrypt ****
        // Encrypt a text file
        var textDataPlain = Encoding.UTF8.GetBytes("Hello! This is a secret message!");
        var textDataEncrypted = await _cryptoManager.EncryptToByteArray(textDataPlain, publicKey);
        await File.WriteAllBytesAsync(Path.Combine(Workspace, SecretTextName), textDataEncrypted);

        // Encrypt a secret image
        await using FileStream imageFileStream =
            new FileStream(Path.Combine(Workspace, SecretImageName), FileMode.Create, FileAccess.Write);
        await _cryptoManager.Encrypt(SecretImage.GetBytes(), imageFileStream, publicKey);
    }
    
    private static async Task Decrypt()
    {
        var privateKeyBytes = await File.ReadAllBytesAsync(Path.Combine(Workspace, PrivateKeyName));
        var privateKey = new RsaKey(privateKeyBytes);
        
        // **** Decrypt ****
        // Decrypt a text file
        var textDataEncrypted = await File.ReadAllBytesAsync(Path.Combine(Workspace, SecretTextName));
        var textDataPlain = await _cryptoManager.DecryptToByteArray(textDataEncrypted, privateKey);
        await File.WriteAllBytesAsync(Path.Combine(Workspace, $"{SecretTextName}.txt"), textDataPlain);

        // Decrypt a secret image
        await using FileStream encryptedImageFileStream =
            new FileStream(Path.Combine(Workspace, SecretImageName), FileMode.Open, FileAccess.Read);
        await using FileStream decryptedImageFileStream =
            new FileStream(Path.Combine(Workspace, $"{SecretImageName}.jpg"), FileMode.Create, FileAccess.Write);
        await _cryptoManager.Decrypt(encryptedImageFileStream, decryptedImageFileStream, privateKey);
    }

    private static async Task Setup()
    {
        // Setup a folder where we can store som files
        var directory = new DirectoryInfo(Workspace);
        if (!directory.Exists)
        {
            directory.Create();
        }

        await GenerateKeysIfNotExist();
    }

    private static async Task GenerateKeysIfNotExist()
    {
        string privateKeyPath = Path.Combine(Workspace, "private.asc");
        if (File.Exists(privateKeyPath))
        {
            // Do not overwrite existing private key
            return;
        }

        var rsaKeys = _cryptoManager.GenerateKeys();

        // Save the keys as files
        await File.WriteAllTextAsync(privateKeyPath, rsaKeys.PrivateKey.ToString());
        await File.WriteAllTextAsync(Path.Combine(Workspace, "public.asc"), rsaKeys.PublicKey.ToString());
    }
}