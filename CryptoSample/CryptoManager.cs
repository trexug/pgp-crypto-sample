﻿using PgpCore;

namespace CryptoSample;

public class CryptoManager
{

    private const string Passphrase = "0000";
    
    public CryptoManager()
    {
        
    }

    public RsaKeyPair GenerateKeys()
    {
        /* 2048 bits = 256 bytes - note that the public/private key byte arrays are larger than 256.
           This is because PGP key contains some metadata + header and because it is base64 encoded */
        const int keyLengthInBits = 2048;

        using PGP pgp = new PGP();
        using MemoryStream privateKeyStream = new MemoryStream();
        using MemoryStream publicKeyStream = new MemoryStream();

        pgp.GenerateKey(
            publicKeyStream,
            privateKeyStream,
            password: Passphrase,
            armor: true, /* True means base64 encoded, false is "raw" bytes + pgp metadata */ 
            strength: keyLengthInBits);

        return RsaKeyPair.Create(publicKeyStream.ToArray(), privateKeyStream.ToArray());
    }

    public async Task<byte[]> EncryptToByteArray(byte[] plainData, RsaKey publicKey)
    {
        using MemoryStream dataStream = new MemoryStream(plainData);
        return await EncryptToByteArray(dataStream, publicKey);
    }
    
    public async Task<byte[]> EncryptToByteArray(Stream inputStream, RsaKey publicKey)
    {
        MemoryStream outputStream = new MemoryStream();
        await Encrypt(inputStream, outputStream, publicKey);
        return outputStream.ToArray();
    }
    
    public async Task Encrypt(byte[] plainData, Stream outputStream, RsaKey publicKey)
    {
        using MemoryStream dataStream = new MemoryStream(plainData);
        await Encrypt(dataStream, outputStream, publicKey);
    }
    
    public async Task Encrypt(Stream inputStream, Stream outputStream, RsaKey publicKey)
        => await Encrypt(inputStream, outputStream, publicKey.Value);

    public async Task Encrypt(Stream inputStream, Stream outputStream, byte[] publicKey)
    {
        /* This is the encryption method which actually does encryption - the rest are overloads for convenience */
        using PGP pgp = new PGP();
        using MemoryStream publicKeyStream = new MemoryStream(publicKey);
        await pgp.EncryptStreamAsync(
            inputStream,
            outputStream,
            publicKeyStream,
            armor: true /* True means base64 encoded, false is "raw" bytes + pgp metadata */);
    }
    
    public async Task<byte[]> DecryptToByteArray(byte[] encryptedData, RsaKey privateKey)
    {
        using MemoryStream dataStream = new MemoryStream(encryptedData);
        return await DecryptToByteArray(dataStream, privateKey);
    }
    
    public async Task<byte[]> DecryptToByteArray(Stream inputStream, RsaKey privateKey)
    {
        MemoryStream outputStream = new MemoryStream();
        await Decrypt(inputStream, outputStream, privateKey);
        return outputStream.ToArray();
    }
    
    public async Task Decrypt(byte[] encryptedData, Stream outputStream, RsaKey privateKey)
    {
        using MemoryStream dataStream = new MemoryStream(encryptedData);
        await Decrypt(dataStream, outputStream, privateKey);
    }
    
    public async Task Decrypt(Stream inputStream, Stream outputStream, RsaKey privateKey)
        => await Decrypt(inputStream, outputStream, privateKey.Value);
    
    public async Task Decrypt(Stream inputStream, Stream outputStream, byte[] privateKey)
    {
        
        
        /* This is the decryption method which actually does decryption - the rest are overloads for convenience */
        using PGP pgp = new PGP();
        using MemoryStream privateKeyStream = new MemoryStream(privateKey);
        EncryptionKeys keys = new EncryptionKeys(privateKeyStream, Passphrase);
        await pgp.DecryptStreamAsync(
            inputStream,
            outputStream,
            keys);
    }
}