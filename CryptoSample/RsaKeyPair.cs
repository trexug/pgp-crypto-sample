﻿namespace CryptoSample;

public readonly struct RsaKeyPair
{
    public RsaKeyPair(RsaKey publicKey, RsaKey privateKey)
    {
        PublicKey = publicKey;
        PrivateKey = privateKey;
    }

    public RsaKey PublicKey { get; }

    public RsaKey PrivateKey { get; }

    public static RsaKeyPair Create(byte[] publicKey, byte[] privateKey)
        => new RsaKeyPair(new RsaKey(publicKey), new RsaKey(privateKey));
}