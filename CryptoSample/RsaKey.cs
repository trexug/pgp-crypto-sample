﻿using System.Text;
namespace CryptoSample;

public readonly struct RsaKey
{
    public RsaKey(byte[] value)
    {
        Value = value;
    }

    public byte[] Value { get; }

    public override string ToString()
        => Value != null
            ? Encoding.ASCII.GetString(Value)
            : string.Empty;
}